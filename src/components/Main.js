import React from 'react';
import List from './List';

function Main() {
    let categories = ["SciFi", "Horror", "Action", "Comedy", "Romance"]

    return(
        <div>
            <List category={categories}/>
        </div>
    )

}
export default Main;